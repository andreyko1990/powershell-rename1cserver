$InputFiles = Get-Item "C:\Users\*\AppData\Roaming\1C\1CEStart\ibases.v8i"
$OldString  = '192.168.0.1'
$NewString  = '10.0.0.1'

$InputFiles | ForEach {
    "start to replace " + $_.FullName 
    $OriginName = $_.FullName 
    $CopyName = ($_.DirectoryName+"\old_"+$_.Name) # you can edit old_ to another copy name
    Copy-Item -Path $OriginName -Destination $CopyName # make copy
    (Get-Content -Path $CopyName).Replace($OldString,$NewString) | out-file -encoding utf8 -FilePath $OriginName # replace string, write to original from copy in utf-8
}